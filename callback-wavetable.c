#include "callback.h"

int patestCallback(const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo *timeInfo, PaStreamCallbackFlags statusFlags, void *userData)
{
	paTestData *data = (paTestData*) userData;
	float *out = (float*)outputBuffer;
	(void) inputBuffer;
	for (unsigned int i = 0; i < framesPerBuffer; ++i) {
		*out++ = data->wavetable[data->int_left_phase];
		*out++ = data->wavetable[data->int_right_phase];
		data->int_left_phase += 1;
		data->int_right_phase += 1;
		if (data->int_left_phase >= TABLE_SIZE) data->int_left_phase -= TABLE_SIZE;
		if (data->int_right_phase >= TABLE_SIZE) data->int_right_phase -= TABLE_SIZE;
	}
	return paContinue;
}

