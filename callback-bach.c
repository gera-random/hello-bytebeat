#include <limits.h>
#include "callback.h"
#include <stdio.h>
#include <math.h>

// "G minor Bach" by skurk, non-cos version
char song[] = "B*918/916-918/91B*918/916-918/91>*;2:1;26/;2:1;2>*;2:1;26/;2:1;2A*;291;28/;291;2A*;291;28/;291;2B*=-;,=-91=-;,=-B*=-;,=-91=-;,=-E*>6=4>692>6=4>6E*>6=4>692>6=4>6D*<3:1<380<3:1<3D*<3:1<380<3:1<3D(=4<3=481=4<3=4D(=4<3=481=4<3=4B(:18/:16.:18/:1B(:18/:16.:18/:1B&;2:1;26/;2:1;2B&;2:1;26/;2:1;2@&;,9*;,8/;,9*;,@&;,9*;,8/;,9*;,@%=-;,=-91=-;,=-@%=-;,=-91=-;,=->*=-;,=-92=-;,=->*=-;,=-92=-;,=->,8/6-8/428/6-8/>,8/6-8/428/6-8/=-412/4192412141=-412/4192412141;-6341613/634163;-6341613/634163;,8/6-8/528/6-8/;,8/6-8/528/6";

static char song_sample(int t) {
	return 30*t*pow(2, ((double)song[(t>>9)])/12-7);
}

int patestCallback(const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo *timeInfo, PaStreamCallbackFlags statusFlags, void *userData)
{
	paTestData *data = (paTestData*) userData;
	char *out = (char*)outputBuffer;
	(void) inputBuffer;
	for (unsigned int i = 0; i < framesPerBuffer; ++i) {
		char b = song_sample(data->int_left_phase);
		*out++ = b;
		*out++ = b;
		++data->int_left_phase;
		++data->int_right_phase;
	}
	return paContinue;
}


