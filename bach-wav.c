#include <stdio.h>
#include <math.h>

// wav header and emit functions are taken from
// https://github.com/skeeto/sort-circle

static void
emit_u32le(unsigned long v, FILE *f)
{
    fputc((v >>  0) & 0xff, f);
    fputc((v >>  8) & 0xff, f);
    fputc((v >> 16) & 0xff, f);
    fputc((v >> 24) & 0xff, f);
}

static void
emit_u32be(unsigned long v, FILE *f)
{
    fputc((v >> 24) & 0xff, f);
    fputc((v >> 16) & 0xff, f);
    fputc((v >>  8) & 0xff, f);
    fputc((v >>  0) & 0xff, f);
}

static void
emit_u16le(unsigned v, FILE *f)
{
    fputc((v >> 0) & 0xff, f);
    fputc((v >> 8) & 0xff, f);
}

static void
emit_u8le(unsigned v, FILE *f)
{
    fputc((v >> 0) & 0xff, f);
}

static FILE *
wav_init(const char *file)
{
    FILE *f = fopen(file, "wb");
    if (f) {
        emit_u32be(0x52494646UL, f); // "RIFF"
        emit_u32le(0xffffffffUL, f); // file length
        emit_u32be(0x57415645UL, f); // "WAVE"
        emit_u32be(0x666d7420UL, f); // "fmt "
        emit_u32le(16,           f); // struct size
        emit_u16le(1,            f); // PCM
        emit_u16le(1,            f); // mono
        emit_u32le(8000,         f); // sample rate (i.e. 44.1 kHz)
        emit_u32le(8000,         f); // byte rate
        emit_u16le(1,            f); // block size
        emit_u16le(8,            f); // bits per sample
        emit_u32be(0x64617461UL, f); // "data"
        emit_u32le(0xffffffffUL, f); // byte length
    }
    return f;
}

// "G minor Bach" by skurk, non-cos version
char song[] = "B*918/916-918/91B*918/916-918/91>*;2:1;26/;2:1;2>*;2:1;26/;2:1;2A*;291;28/;291;2A*;291;28/;291;2B*=-;,=-91=-;,=-B*=-;,=-91=-;,=-E*>6=4>692>6=4>6E*>6=4>692>6=4>6D*<3:1<380<3:1<3D*<3:1<380<3:1<3D(=4<3=481=4<3=4D(=4<3=481=4<3=4B(:18/:16.:18/:1B(:18/:16.:18/:1B&;2:1;26/;2:1;2B&;2:1;26/;2:1;2@&;,9*;,8/;,9*;,@&;,9*;,8/;,9*;,@%=-;,=-91=-;,=-@%=-;,=-91=-;,=->*=-;,=-92=-;,=->*=-;,=-92=-;,=->,8/6-8/428/6-8/>,8/6-8/428/6-8/=-412/4192412141=-412/4192412141;-6341613/634163;-6341613/634163;,8/6-8/528/6-8/;,8/6-8/528/6";

char song_sample(int t) {
	return 30*t*pow(2, ((double)song[(t>>9)])/12-7);
}

int main(int argc, char **argv)
{
	if (argc != 2) {
		printf("usage: %s file.wav\n", argv[0]);
		return 1;
	}
	FILE *f = wav_init(argv[1]);
	//for (int t = 0; t < 100000; ++t)
	int t = 0;
	while(t>>9 <= sizeof(song)) 
		emit_u8le(song_sample(t++), f);
	fclose(f);
}
