#pragma once
#include <portaudio.h>

#define TABLE_SIZE 200

typedef struct
{
	float left_phase;
	float right_phase;
	int int_left_phase;
	int int_right_phase;
	float wavetable[TABLE_SIZE];
}
paTestData;

int patestCallback(const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo *timeInfo, PaStreamCallbackFlags statusFlags, void *userData);
